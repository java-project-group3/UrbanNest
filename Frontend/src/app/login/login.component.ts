declare var google:any;
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  protected aFormGroup: FormGroup;
  loginForm: FormGroup;
  userName:any;
  userId:any
user : any;
isCaptchaValid: boolean = false;
captchaResolved: boolean = false;
siteKey: string = "6Lev5GcpAAAAAF-jW9OzsG-Im3j0aQE1nBxAVyhf";
  constructor(private formBuilder: FormBuilder,private service :UserService,private router :Router,private toastr:ToastrService) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }
  ngOnInit(): void {
    google.accounts.id.initialize({
      client_id:'156486590498-mkq1iog6cjpsvkbnao31qc1diq9fr6ne.apps.googleusercontent.com',
      callback: (resp :any)=>this.handleLogin(resp)
        
      
  
      });
  
      google.accounts.id.renderButton(document.getElementById("google-btn"),{
      theme :'filled_blue',
      size : 'large',
      shape:'rectangle',
      width:350,
      })
  
  }
  

   async loginSubmit() {
    const email = this.loginForm.get('email')?.value;
    const password = this.loginForm.get('password')?.value;
  
    if (email && password) {
      if (email.errors) {
        if (email.hasError('required')) {
          // Handle required error for email
        } else if (email.hasError('email')) {
          // Handle email format error
        }
      }
  
      if (password.errors) {
        if (password.hasError('required')) {
          // Handle required error for password
        } else if (password.hasError('minlength')) {
          // Handle minimum length error for password
        }
      }
    }
    console.log('Email:', email);
console.log('Password:', password);
    
if(email=='admin@gmail.com'&&password=='admin@123'){
  this.service.setIsAdminLoggedIn();
  this.service.setIsUserLoggedIn();
  localStorage.setItem("emailId",email)
  this.router.navigate(['admin']);
}
else{
  this.user=null;
    await this.service.userLogin(email, password).then((data: any) => {
      console.log(data);
      this.user = data;
      console.log('API Response:', data);
      this.userName=this.service.setUserName(this.user.firstName);
      console.log(this.user.userId)
      this.service.setUserId(this.user.userId);
      this.userId=this.service.getUserId();
      console.log(this.userId)
    });
   
    if (this.user!= null) {
      this.service.setIsUserLoggedIn();        
  
      this.toastr.success("Login Success");
      this.router.navigate(['pg']);
    } else {
      if(this.user === null){
        console.log('No User exists!');
        this.toastr.error('No user Exists Please Register and login');
      }
      else

{
  this.toastr.error("Invalid Credentials");
}
      
    }
  }
  }
  onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }

  private decodeToken(token:string){
    return JSON.parse(atob(token.split(".")[1]));
  }

 handleLogin(response :any){
  console.log("working");
  if(response){
    const payLoad = this.decodeToken(response.credential)
    sessionStorage.setItem("loggedInUser",JSON.stringify(payLoad));
    localStorage.setItem("email",payLoad.email);
    this.service.setIsUserLoggedIn();
    this.router.navigate(['pg']);
  }
 }
}
