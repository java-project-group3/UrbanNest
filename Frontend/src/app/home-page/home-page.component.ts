import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  pgs: any[] = [
    { id: 1, image: 'assets/Images/one.avif', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 2, image: 'assets/Images/two.webp', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 3, image: 'assets/Images/three.webp', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 4, image: 'assets/Images/four.avif', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 5, image: 'assets/Images/five.JPG', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 6, image: 'assets/Images/six.jpeg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 7, image: 'assets/Images/seven.avif', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 8, image: 'assets/Images/eight.jpeg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 9, image: 'assets/Images/nine.webp', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 10, image: 'assets/Images/ten.webp', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 11, image: 'assets/Images/eleven.avif', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 12, image: 'assets/Images/tweleve.webp', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 13, image: 'assets/Images/thirteen.webp', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 14, image: 'assets/Images/fourteen.avif', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 15, image: 'assets/Images/fifteen.avif', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 16, image: 'assets/Images/sixteen.avif', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 17, image: 'assets/Images/seventeen.jpg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 18, image: 'assets/Images/eighteen.jpeg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 19, image: 'assets/Images/nineteen.webp', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 20, image: 'assets/Images/twenty.jpeg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 21, image: 'assets/Images/twentyone.jpeg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 22, image: 'assets/Images/twentytwo.jpg', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 23, image: 'assets/Images/twentythree.jpeg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 24, image: 'assets/Images/twentyfour.jpeg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 25, image: 'assets/Images/twentyfive.jpg', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 26, image: 'assets/Images/twentysix.webp', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 27, image: 'assets/Images/twentyseven.webp', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 28, image: 'assets/Images/twentyeight.webp', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 29, image: 'assets/Images/twentynine.avif', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 30, image: 'assets/Images/thirty.jpeg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 31, image: 'assets/Images/thirtyone.webp', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 32, image: 'assets/Images/thirtytwo.avif', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 33, image: 'assets/Images/thirtythree.jpg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 34, image: 'assets/Images/thirtyfour.jpeg', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 35, image: 'assets/Images/thirtyfive.jpg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 36, image: 'assets/Images/thirtysix.jpg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 37, image: 'assets/Images/thirtyseven.jpg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 38, image: 'assets/Images/thirtyeight.jpg', name: 'PG 1', address: 'Address 1', gender: 'Male', shares: '2 share', contact: '1234567890' },
    { id: 39, image: 'assets/Images/thirtynine.jpg', name: 'PG 2', address: 'Address 2', gender: 'Female', shares: '3 share', contact: '9876543210' },
    { id: 40, image: 'assets/Images/fouty.webp', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 41, image: 'assets/Images/foutyone.avif', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    { id: 42, image: 'assets/Images/foutytwo.jpg', name: 'PG 3', address: 'Address 3', gender: 'Male', shares: '4 share', contact: '4561237890' },
    

// Add more PGs as needed
  ];
  selectedPg: any;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  showContactDetails(pg: any): void {
    this.selectedPg = pg;
    // You can implement this method to show contact details in detail
    // For example, navigate to another component or open a modal with contact details
    console.log('Contact details:', pg.contact);
  }
  closeModal(): void {
    this.selectedPg = null;
  }
}
