import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';

import { CommonModule } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha';
import { FooterComponent } from './footer/footer.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { HomePageComponent } from './home-page/home-page.component';

import { HomeComponent } from './home/home.component';
import { PasswordComponent } from './password/password.component';
import { ForgotComponent } from './forgot/forgot.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { PgComponent } from './pg/pg.component';
import { PgDetailsComponent } from './pg-details/pg-details.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ContactDetailsDialogComponent } from './contact-details-dialog/contact-details-dialog.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { LoginMComponent } from './login-m/login-m.component';
import { RegistrationMComponent } from './registration-m/registration-m.component';
import { ManagersComponent } from './managers/managers.component';

import { UsersComponent } from './users/users.component';
import { AdminComponent } from './admin/admin.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { CartComponent } from './cart/cart.component';
import { UpdatepgComponent } from './updatepg/updatepg.component';
import { AddpgsComponent } from './addpgs/addpgs.component';
import { BookpgComponent } from './bookpg/bookpg.component';







@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    LogoutComponent,
    RegisterComponent,
    FooterComponent,
    AboutusComponent,
  
    HomePageComponent,
    HomeComponent,
    PasswordComponent,
    ForgotComponent,
    NavbarComponent,
    PgComponent,
    PgDetailsComponent,
    ContactDetailsDialogComponent,
    LoginMComponent,
    RegistrationMComponent,
    ManagersComponent,

    UsersComponent,
    AdminComponent,
    ManagerDashboardComponent,
    CartComponent,

    UpdatepgComponent,
    AddpgsComponent,
    BookpgComponent,

    

  
    

 
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
  
    CommonModule,
  
    BrowserAnimationsModule,
    
    HttpClientModule,
    NgxCaptchaModule,
    MatMenuModule,
    MatFormFieldModule,
    MatButtonModule,
    CarouselModule.forRoot()
  
  ],
  providers: [
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
