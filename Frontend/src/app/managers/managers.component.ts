import { Component } from '@angular/core';
import { UserService } from '../user.service';
@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrl: './managers.component.css'
})
export class ManagersComponent {
  managers: any;
  emailId: any;
  editManager: any;

  constructor(private userService: UserService) {
   
  }

  ngOnInit() {
    this.userService.getManagers().subscribe((data: any) => {
      this.managers = data;
    });
  }

  

  deleteManagerById(manager: any) {
    this.userService.deleteManagerById(manager.managerId).subscribe((data: any) => {
      console.log(data);
    });

    const index = this.managers.findIndex((element: any) => {
      return element.managerId == manager.managerId;
    });

    this.managers.splice(index, 1);

      }

}
