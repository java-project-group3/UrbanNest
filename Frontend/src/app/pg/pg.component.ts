import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
interface PG {
  pgId: number;
  imageName: string;
  subImage1:string;
  subImage2:string;
  licenseNumber: string;
  name: string;
  gender: string;
  location: string;
  sharesAvailable: string;
  priceForFour?: string;
  priceForTwo?: string;
  priceForThree?: string;
  vacancies2Share?: number;
  vacancies3Share?: number;
  vacancies4Share?: number;
  totalVacancies?:number;

}


@Component({
  selector: 'app-pg',
  templateUrl: './pg.component.html',
  styleUrl: './pg.component.css'
})
export class PgComponent {
  pgs: PG[] = [];

  constructor(private service:UserService,private router :Router) {}

  ngOnInit(): void {
    this.service.getPgs().subscribe(
      (pgs: PG[]) => {
        this.pgs = pgs;
      },
      (error) => {
        console.error('Error fetching PG data:', error);
      }
    );
  }
  viewDetails(pgId: number): void {
    this.router.navigate(['pgdetails', { pgId: pgId }]); // Pass pgId correctly
  }
    

}
