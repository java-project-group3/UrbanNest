import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
declare var jQuery: any; // Declare jQuery

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  
  users: any;
  emailId: any;
  mobileNumber:any;
  gender:any;
  address:any;
  editUsers: any;       //for 2-way databinding with dialog box


  constructor(private service: UserService) {
    this.emailId = localStorage.getItem('emailId');

    //for 2-way databinding with dialog box
    this.editUsers = {
      c: '',
      userName: '',
      gender: '',
      emailId: '',   
    };
  }

  ngOnInit() {
    this.service.getAllUsers().subscribe((data: any) => { this.users = data; });
  }

  editUser(user: any) {
    console.log(this.users);

    // Employee data binding to the editEmp variable for 2-way databinding
    // Launching the Modal Dialog Box
    jQuery('#myModal').modal('show');
  }

  updateUser() {
    console.log(this.editUsers);
    this.service.updatUser(this.editUsers).subscribe((data: any) => { console.log(data); });
  }

  deleteUser(user: any) {
    this.service.deleteUser(user.userid).subscribe((data: any) => {console.log(data);});

    const i = this.users.findIndex((element: any) => {
      return element.userId == user.userId;
    });

    this.users.splice(i, 1);

    alert('User Deleted Successfully!!!');
  }
}