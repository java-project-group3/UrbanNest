import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false; // Flag to track user authentication state
  pgs: any[] = [
    { name: 'PG 1', address: 'Address 1', shares: '2 share', contact: '1234567890' },
    { name: 'PG 2', address: 'Address 2', shares: '3 share', contact: '9876543210' },
    { name: 'PG 3', address: 'Address 3', shares: '4 share', contact: '4561237890' },
    { name: 'PG 1', address: 'Address 1', shares: '2 share', contact: '1234567890' },
    { name: 'PG 2', address: 'Address 2', shares: '3 share', contact: '9876543210' },
    { name: 'PG 3', address: 'Address 3', shares: '4 share', contact: '4561237890' },
    { name: 'PG 1', address: 'Address 1', shares: '2 share', contact: '1234567890' },
    { name: 'PG 2', address: 'Address 2', shares: '3 share', contact: '9876543210' },
    { name: 'PG 3', address: 'Address 3', shares: '4 share', contact: '4561237890' },
    { name: 'PG 1', address: 'Address 1', shares: '2 share', contact: '1234567890' },
    { name: 'PG 2', address: 'Address 2', shares: '3 share', contact: '9876543210' },
    { name: 'PG 3', address: 'Address 3', shares: '4 share', contact: '4561237890' },
    { name: 'PG 1', address: 'Address 1', shares: '2 share', contact: '1234567890' },
    { name: 'PG 2', address: 'Address 2', shares: '3 share', contact: '9876543210' },
    { name: 'PG 3', address: 'Address 3', shares: '4 share', contact: '4561237890' }
    // Add more PGs as needed
  ];
  constructor(private router: Router, private userService: UserService) {}
  ngOnInit(): void {
    // Check if the user is already logged in
    this.isLoggedIn = this.userService.isUserLoggedIn();
  }
  logout(): void {
    // Call the logout method from UserService
    this.userService.logout();

    // Redirect the user to the header page
    this.router.navigate(['/']);
  }

}
