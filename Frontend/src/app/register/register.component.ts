import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user: any;
  siteKey: string = '6LeJjWgpAAAAALvszYULjRzlg7eUUo_49Q_wsHnO';

  constructor(
    private router: Router,
    private service: UserService,
    private toastr: ToastrService // Inject ToastrService
  ) {
    this.user = {
      firstName: '',
      lastName: '',
      emailId: '',
      password: '',
      mobileNumber: '',
      gender: '',
      address: '',
      confirmPassword: ''
    };
  }

  ngOnInit() {
    console.log('RegisterComponent initialized.');
  }

  registerSubmit(regForm: any) {
    console.log('Registration Form Submitted');
    console.log('First Name:', this.user.firstName);
    console.log('Last Name:', this.user.lastName);
    console.log('Address:', this.user.address);
    console.log('Email:', this.user.emailId);
    console.log('Phone Number:', this.user.mobileNumber);
    console.log('Gender:', this.user.gender);
    console.log('Password:', this.user.password);
    console.log('Confirm Password:', this.user.confirmPassword);

    if (!this.user.recaptcha) {
      console.log('Please complete the reCAPTCHA.');
      return;
    }

    if (this.user.password !== this.user.confirmPassword) {
      console.log('Password and Confirm Password must be the same.');
      return; // Stop execution if passwords don't match
    }

    // Call your service method or perform any necessary actions here
    this.service.registerUser(this.user).subscribe(
      (data: any) => { 
        console.log('Registration successful:', data); 
        this.toastr.success('Registration Successful', 'Success'); // Display toastr message on success
        this.router.navigate(['login']); // Redirect to login page after successful registration
      },
      (error: any) => {
        this.toastr.error('Registration Failed', 'Error');
        console.error('Registration failed:', error);
        // Handle registration error here
      }
    );
  }
  handleReset() {
    console.log('reCAPTCHA reset.');
  }

  handleExpire() {
    console.log('reCAPTCHA expired.');
  }

  handleLoad() {
    console.log('reCAPTCHA loaded.');
  }

  handleSuccess(event: any) {
    console.log('reCAPTCHA success.');
    this.user.recaptcha = event;
  }
}
