import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { BookpgComponent } from '../bookpg/bookpg.component';
import { PgComponent } from '../pg/pg.component';
import { PG } from '../pg-details/pg-details.component';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {

  emailId: any;

  cartItems: any;
  pg: PG[];
  userId:any;

  

  constructor(private service: UserService, private router: Router,private dialog: MatDialog,private toastr:ToastrService) {
    
    
  

    this.pg = service.getCartItems();

    this.pg.forEach((element: any) => {

      // this.total = this.total + parseInt(element.price);

    });
  }
  ngOnInit() {
    this.userId=this.service.getUserId();
    this.service.getCart(this.userId).subscribe(
      (cartItems:any)=>{
        this.cartItems=cartItems;
        console.log(cartItems);
        console.log(cartItems.length)
        this.service.setCartLength(this.cartItems.length)
       
        
        
      }
    )
  }

  deleteCartProduct(pg: any) {
    const i = this.pg.findIndex((element: any) => {
      return element.id == pg.id;
    });
   
 
    
  }
  openBookPgDialog(pg: PG): void {
    const dialogRef = this.dialog.open(BookpgComponent, {
      width: '400px',
      height: 'auto',
      data: { pg: pg } 
    });
  }
  
  
  }
  
  
  



