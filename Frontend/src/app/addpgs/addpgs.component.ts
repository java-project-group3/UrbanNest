import { Component, Inject } from '@angular/core';
import { PG } from '../pg-details/pg-details.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addpgs',
  templateUrl: './addpgs.component.html',
  styleUrls: ['./addpgs.component.css']
})
export class AddpgsComponent {
  pg: PG = {
    pgId: 0,
    imageName: '',
    subImage1: '',
    subImage2: '',
    licenseNumber: '',
    amenities: '',
    name: '',
    gender: '',
    location: '',
    sharesAvailable: '',
    manager: {
      managerId: 0,
      firstName: '',
      lastName: '',
      emailId: '',
      mobileNumber: 0,
      address: ''
    }
  };

  constructor(
    public dialogRef: MatDialogRef<AddpgsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: UserService,
    private toaster: ToastrService
  ) {
    const managerId = this.service.getManagerId();
    if (managerId !== undefined) {
      if (this.pg.manager !== undefined) {
        this.pg.manager.managerId = managerId;
      } else {
        console.error('Manager ID is undefined');
      }
    }
  }

  onSubmit() {

    this.service.addPg(this.pg).subscribe(
      response => {
        console.log('PG added successfully:', response);
        this.toaster.success('Pg added Successfully');
        this.dialogRef.close();
      },
      error => {
        console.error('Error adding PG:', error);
        this.toaster.error('Error Adding pg Details');
      }
    );
  }
}
