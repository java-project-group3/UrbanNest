import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrl: './password.component.css'
})
export class PasswordComponent implements OnInit {

  emailId: string = '';
  users:any;


  constructor(private service: UserService,private toastr: ToastrService,private router: Router) {
    this.users= {
      emailId: '',
      password: ''
    };
  }

  ngOnInit() {
    // Subscribe to changes in the DataService
    this.service.currentEmailId.subscribe(emailId => {
      this.emailId = emailId;
    });
  }

  passwordChanged(regForm: any) {
      this.users.emailId = regForm.emailId;
      this.users.password = regForm.password;

      console.log(  this.users.emailId);
      console.log(  this.users.password);

      this.service.updateUserPassword(this.users).subscribe((data: any) => { console.log(data); });
       this.toastr.success('Password changed Successfully!');
      this.router.navigate(['login']);
    }
   
  }