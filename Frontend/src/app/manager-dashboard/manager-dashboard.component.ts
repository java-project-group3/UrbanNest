import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UpdatepgComponent } from '../updatepg/updatepg.component';
import { AddpgsComponent } from '../addpgs/addpgs.component';

interface Manager {
  managerId: number;
  firstName: string;
  lastName: string;
  emailId: string;
  mobileNumber: number;
  address: string;
}

interface PG {
  pgId: number;
  imageName: string;
  subImage1: string;
  subImage2: string;
  licenseNumber: string;
  amenities: string;
  name: string;
  gender: string;
  location: string;
  sharesAvailable: string;
  priceForFour?: string;
  priceForTwo?: string;
  priceForThree?: string;
  vacancies2Share?: number;
  vacancies3Share?: number;
  vacancies4Share?: number;
  totalVacancies?: number;
  managerId: number;
  manager?: Manager; 
}

@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.css']
})
export class ManagerDashboardComponent implements OnInit {
  pgs: PG[] = [];
  managerId: number | undefined; 

  constructor(private userService: UserService, private route: ActivatedRoute, private dialog: MatDialog) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.managerId = +params['id'];
      if (this.managerId) {
        this.fetchPGs(); 
      }
    });
  }

  fetchPGs() {
    this.userService.getPgsByManagerId(this.managerId).subscribe((data: any) => {
      this.pgs = data;
      console.log(this.pgs);
    });
  }

  openUpdateDialog(pg: PG): void {
    const dialogRef = this.dialog.open(UpdatepgComponent, {
      width: '600px',
      height: 'auto',
      data: { pg: pg } 
    });
  }

  addNewPG() {

    const dialogRef = this.dialog.open(AddpgsComponent, {
      width: '850px',
      height: 'auto'
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result === 'added') {
        this.fetchPGs(); 
      }
    });
  }
}
