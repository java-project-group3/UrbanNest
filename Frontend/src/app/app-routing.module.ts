import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { AboutusComponent } from './aboutus/aboutus.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HomeComponent } from './home/home.component';
import { ForgotComponent } from './forgot/forgot.component';
import { PasswordComponent } from './password/password.component';
import { PgDetailsComponent } from './pg-details/pg-details.component';
import { PgComponent } from './pg/pg.component';
import { LoginMComponent } from './login-m/login-m.component';
import { ManagersComponent } from './managers/managers.component';
import { UsersComponent } from './users/users.component';
import { AdminComponent } from './admin/admin.component';
import { CartComponent } from './cart/cart.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { RegistrationMComponent } from './registration-m/registration-m.component';

const routes: Routes = [
  //s {path : '' , component:LoginComponent},
  { path: 'pgdetails/:pgId', component: PgDetailsComponent },
  { path: 'pgdetails', component: PgDetailsComponent },
  { path: 'pg', component: PgComponent },
  { path: '', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'loginm', component: LoginMComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'homep', component: HomePageComponent },
  { path: 'home', component: HomeComponent },
  { path: 'forgotpassword', component: ForgotComponent },
  { path: 'password', component: PasswordComponent },
  { path: 'manager', component: ManagersComponent },
  { path: 'users', component: UsersComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'cart', component: CartComponent },
  { path: 'managerdb/:id', component: ManagerDashboardComponent },
  { path: 'registerm', component: RegistrationMComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
