import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-contact-details-dialog',
  templateUrl: './contact-details-dialog.component.html',
  styleUrls: ['./contact-details-dialog.component.css']
})
export class ContactDetailsDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ContactDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

}
