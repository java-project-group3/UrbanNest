import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AddpgsComponent } from '../addpgs/addpgs.component';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  loginStatus: any; // Initialize loginStatus as a boolean
  adminLoginStatus: any;
  managerLoginStatus: any;
  cartItems: any;
  userName:any;
  length:any;

  constructor(private service: UserService, private router: Router, private dialog: MatDialog) {
    this.cartItems = service.getCartItems();
    
  }

  ngOnInit() {
    // Listen for changes in login status
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
      this.userName=this.service.getUserName();
      this.length=this.service.getCartLength();
      console.log(this.length)
    });

    // Listen for changes in admin login status
    this.service.getAdminLoginStatus().subscribe((data: any) => {
      this.adminLoginStatus = data;
    });

    // Listen for changes in manager login status
    this.service.getmanagerLoginStatus().subscribe((data: any) => {
      this.managerLoginStatus = data;
    });
  }

  openAddPgsDialog(): void {
    const dialogRef = this.dialog.open(AddpgsComponent, {
      width: '850px',
      height: 'auto'
    

    });


  }

  navigateToUserLogin(): void {
    this.router.navigateByUrl('login'); // Assuming 'login' is the route path for your login component
  }

  navigateToHome(): void {
    this.router.navigateByUrl('home');
    this.service.setIsUserLoggedOut();
    this.service.setIsManagerLoggedOut();
  }

  navigateToCart(): void {
    this.router.navigateByUrl('cart');
  }

  navigateToManagerLogin(): void {
    this.router.navigateByUrl('loginm'); // Assuming 'managerlogin' is the route path for your manager login component
  }
  logOut() {

  }

}
