import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PG } from '../pg-details/pg-details.component';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';

@Component({
  selector: 'app-bookpg',
  templateUrl: './bookpg.component.html',
  styleUrls: ['./bookpg.component.css']
})
export class BookpgComponent {
  pg: PG;

  constructor(
    public dialogRef: MatDialogRef<BookpgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastr: ToastrService,
    private service: UserService
  ) {
    this.pg = data.pg;
  }

  bookPg() {
    const selectedShareElement = document.getElementById('selectShare') as HTMLSelectElement;
    const selectedShare = selectedShareElement ? Number(selectedShareElement.value) : undefined;

    if (selectedShare === 2 && this.pg && this.pg.vacancies2Share !== undefined && this.pg.vacancies2Share > 0) {
      this.service.updateVacancies2Share(this.pg.name).subscribe(
        (updatedPg: PG) => {
          this.pg = updatedPg;
          this.dialogRef.close(this.pg);
          this.toastr.success('PG booked!');
        },
        error => {
          console.error('Error updating vacancies for two share:', error);
          this.toastr.error('Error booking PG. Please try again later.');
        }
      );
    } else if (selectedShare === 3 && this.pg && this.pg.vacancies3Share !== undefined) {
      this.service.updateVacancies3Share(this.pg.name).subscribe(
        (updatedPg: PG) => {
          this.pg = updatedPg;
          this.dialogRef.close(this.pg);
          this.toastr.success('PG booked!');
        },
        error => {
          console.error('Error updating vacancies for three share:', error);
          this.toastr.error('Error booking PG. Please try again later.');
        }
      );
    } else if (selectedShare === 4 && this.pg && this.pg.vacancies4Share !== undefined) {
      this.service.updateVacancies4Share(this.pg.name).subscribe(
        (updatedPg: PG) => {
          this.pg = updatedPg;
          this.dialogRef.close(this.pg);
          this.toastr.success('PG booked!');
        },
        error => {
          console.error('Error updating vacancies for four share:', error);
          this.toastr.error('Error booking PG. Please try again later.');
        }
      );
    } else {
      this.toastr.error('Vacancies are not available for the selected share.');
    }
  }
}
