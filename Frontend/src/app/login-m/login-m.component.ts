import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login-m',
  templateUrl: './login-m.component.html',
  styleUrls: ['./login-m.component.css']
})
export class LoginMComponent {
  email: string | undefined;
  password: string | undefined;
  managerId: number | undefined;
  constructor(private router: Router, private service: UserService, private toastr: ToastrService) {}

  async onSubmit() {
    try {
      const response = await this.service.managerLogin(this.email, this.password);
      console.log(response);
      console.log(response.managerId);
      this.service.setManagerId(response.managerId);
      localStorage.setItem('managerDetails', JSON.stringify(response));
      console.log('managerDetails', response);
      this.managerId = response.managerId; // Assign managerId here
      this.service.setIsManagerLoggedIn();
      this.service.setIsUserLoggedIn();
      this.router.navigateByUrl(`managerdb/${this.managerId}`); // Use managerId here
    } catch (error) {
      this.toastr.error('Invalid email or password.');
    }
  }
}
