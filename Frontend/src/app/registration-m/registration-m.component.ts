import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration-m',
  templateUrl: './registration-m.component.html',
  styleUrls: ['./registration-m.component.css']
})
export class RegistrationMComponent implements OnInit {
  manager: any;
  siteKey: string = '6LeJjWgpAAAAALvszYULjRzlg7eUUo_49Q_wsHnO';
  formSubmitted: boolean = false;
  recaptchaCompleted: boolean = false;

  constructor(
    private router: Router,
    private service: UserService,
    private toastr: ToastrService
  ) {
    this.manager = {
      managerid: '',
      address: '',
      emailId: '',
      firstName: '',
      lastName: '',
      password: '',
      mobileNumber: '',
      gender: '',
      confirmPassword: ''
    };
  }

  ngOnInit() {
    console.log('RegisterComponent initialized.');
  }

  registerSubmit(regForm: any) {
    console.log('Registration Form Submitted');
    console.log(regForm);

    // if (regForm.invalid || !this.recaptchaCompleted) {
    //   console.log('Invalid form or reCAPTCHA not completed.');
    //   return;
    // }

  
    this.service.registerManager(this.manager).subscribe(
      (data: any) => {
        console.log('Registration successful:', data);
        this.toastr.success('Registration Successful', 'Success');
        this.formSubmitted = true;
        this.router.navigate(['login-m']);
      },
      (error: any) => {
        this.toastr.error('Registration Failed', 'Error');
        console.error('Registration failed:', error);
        this.formSubmitted = false;
      }
    );
  }

  handleReset() {
    console.log('reCAPTCHA reset.');
  }

  handleExpire() {
    console.log('reCAPTCHA expired.');
  }

  handleLoad() {
    console.log('reCAPTCHA loaded.');
  }

  handleSuccess(event: any) {
    console.log('reCAPTCHA success.');
    this.recaptchaCompleted = true; // Set recaptchaCompleted to true when reCAPTCHA is completed
  }
}
