import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrl: './forgot.component.css'
})
export class ForgotComponent implements OnInit {

  otp1: any;  
  formModel: any = {};

  // Dependency Injection for EmpService
  constructor(private service: UserService,private toastr: ToastrService,private router: Router) {
  }

  ngOnInit() {
  }

  getOtp() {
    this.service.getUserotp(this.formModel.emailId).subscribe(
      (data: any) => {
        this.otp1 = data.otp;  
        console.log(this.otp1);
        this.toastr.success('Otp Sent to Registered Mail')
      },
      (error: any) => {
        console.error('Error fetching workers:', error);
      }
    );

    this.service.changeEmailId(this.formModel.emailId);
  }

  loginSubmit(loginForm: any) {
    if (loginForm.otp == this.otp1) {
      console.log("hii");
     this.router.navigate(['password']);
    }
    else{
      this.toastr.error("Invalid Credentials");
    }
  }
}