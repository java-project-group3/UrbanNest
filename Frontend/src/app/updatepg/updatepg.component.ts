import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PG } from '../pg-details/pg-details.component';
import { UserService } from '../user.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-updatepg',
  templateUrl: './updatepg.component.html',
  styleUrls: ['./updatepg.component.css']
})
export class UpdatepgComponent {
  pg: PG;

  constructor(
    public dialogRef: MatDialogRef<UpdatepgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: UserService,
    private toaster: ToastrService
  ) {
    this.pg = data.pg;
  }

  updatePg(pg: PG): void {
    // Calculate total vacancies
    const totalVacancies = (pg.vacancies2Share ?? 0) + (pg.vacancies3Share ?? 0) + (pg.vacancies4Share ?? 0);
    // Set total vacancies
    pg.totalVacancies = totalVacancies;

    // Make the API call to update PG details
    this.service.updatePg(pg).pipe(
      catchError((error: any) => {
        console.error('Error updating PG details:', error);
        this.toaster.error('Error updating PG details');
        return throwError(error);
      })
    ).subscribe(
      (response: any) => {
        console.log('PG details updated successfully:', response);
        this.toaster.success('PG details updated successfully');
        this.dialogRef.close();
      }
    );
  }
}
