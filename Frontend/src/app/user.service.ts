import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { PG } from './pg-details/pg-details.component';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private cartItems: PG[] = [];
  private emailIdSource = new BehaviorSubject<string>('');
  currentEmailId = this.emailIdSource.asObservable();
  loginStatus: any;
  adminLoginStatus: any;
  managerLoginStatus: any;
  isManagerLoggedIn: any;
  isAdminLoggedIn: any;
  isUserLoggedIn: any;
  private managerId:number|undefined;
  private userName:string|undefined;
  private userId:number|undefined;
  private length:any;
  constructor(private http: HttpClient) {

    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
    this.isAdminLoggedIn = false;
    this.adminLoginStatus = new Subject();
    this.isManagerLoggedIn = false;
    this.managerLoginStatus = new Subject();
  }
  setCartLength(length:any){
    this.length=length;
  }
  getCartLength():any{
    return this.length;
  }

  setUserId(userId:any):void {
    this.userId=userId;
  }
  getUserId(): any  {
return this.userId;
  }
  setManagerId(id: number) {
    this.managerId = id;
  }

  getManagerId(): number | undefined {
    return this.managerId;
  }
  setUserName(userName:string){
    this.userName=userName;
  }
  getUserName():string | undefined{
    return this.userName;

  }
  registerManager(manager: any) {
    return this.http.post('http://localhost:8086/addManager', manager);
  }
  getManagers() {
    return this.http.get('http://localhost:8086/getManager');
  }
  managerLogin(email: any, password: any): any {
    return this.http.get(`http://localhost:8086/managerLogin/${email}/${password}`).toPromise();
  }
  deleteManagerById(managerId: any) {
    return this.http.delete('http://localhost:8086/deleteManagerById/' + managerId)
  }

  getPgs(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8086/getPgs');
  }
  getAllUsers() {
    return this.http.get('http://localhost:8085/getUsers');
  }
  addPg(pg:any){
    return this.http.post('http://localhost:8086/addPg', pg);

  }
  getUserById(userId: any) {
    return this.http.get('http://localhost:8085/getUserById/' + userId);
  }
  getPgById(pgId: any): Observable<any> {
    if (pgId !== undefined) {
      return this.http.get(`http://localhost:8086/getPgById/${pgId}`);
    } else {
      console.error('pgId is undefined');
      return throwError('pgId is undefined'); // or any appropriate error handling
    }
  }
  getPgsByManagerId(managerId: any): Observable<any[]> {
    return this.http.get<any[]>(`http://localhost:8086/getPgsByManagerId/${managerId}`);
  }
  updateVacancies2Share(name: string): Observable<PG> {
    return this.http.put<PG>(`http://localhost:8086/updateVacancies2Share/${name}`, {});
  }
  updateVacancies3Share(name: string): Observable<PG> {
    return this.http.put<PG>(`http://localhost:8086/updateVacancies2Share/${name}`, {});
  }
  updateVacancies4Share(name: string): Observable<PG> {
    return this.http.put<PG>(`http://localhost:8086/updateVacancies2Share/${name}`, {});
  }
  updatUser(user: any) {
    return this.http.put('http://localhost:8085/updateUser', user);
  }
  deleteUser(userId: any) {
    return this.http.delete('http://localhost:8085/deleteUserById/' + userId);
  }
  getuserRegistrationOtp(otp: any): any {
    return this.http.get('http://localhost:8086/fetchUserOtp/' + otp).toPromise();
  }
  registerUser(user: any): any {
    return this.http.post('http://localhost:8085/addUser', user);
  }
  userLogin(email: any, password: any): any {
    return this.http.get(`http://localhost:8085/userLogin/${email}/${password}`).toPromise();
  }
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }
  setIsAdminLoggedIn() {
    this.isAdminLoggedIn = true;
    this.adminLoginStatus.next(true);
  }
  setIsManagerLoggedIn() {
    this.isManagerLoggedIn = true;
    this.managerLoginStatus.next(true);

  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }
  getIsAdminLogged(): boolean {
    return this.isAdminLoggedIn;
  }
  getIsManagerLogged(): boolean {
    return this.isManagerLoggedIn;
  }
  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
  getAdminLoginStatus(): any {
    return this.adminLoginStatus.asObservable();
  }
  getmanagerLoginStatus(): any {
    return this.managerLoginStatus.asObservable();
  }

  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }
  setIsAdminLoggedOut() {
    this.isAdminLoggedIn = false;
    this.adminLoginStatus.next(false);
  }
  setIsManagerLoggedOut() {
    this.isManagerLoggedIn = false;
    this.managerLoginStatus.next(false);
  }
  logout(): void {
    // Clear user authentication state
    localStorage.removeItem('userToken');
    // Add any additional cleanup logic here
  }
  changeEmailId(emailId: string) {
    this.emailIdSource.next(emailId);
  }
  updateUserPassword(users: any): any {
    return this.http.put('http://localhost:8085/updateUserPassword', users);
  }
  getUserotp(emailId: any): any {
    return this.http.get('http://localhost:8085/getUserEmailId/' + emailId);
  }
  addToCart(pg: PG) {
    this.cartItems.push(pg);

  }
  toCart(cart:any){
    return this.http.post('http://localhost:8085/addToCart', cart);
  }
  getCart(userId:any):any{
    return this.http.get('http://localhost:8085/getCartItemsByUserId/' +userId)
  }
  updatePg(pg: any) {
    return this.http.put('http://localhost:8086/updatePg', pg);
  }
  
  getCartItems(): PG[] {
    return this.cartItems;
  }

  
  clearCart() {
    this.cartItems = [];
  }

}


