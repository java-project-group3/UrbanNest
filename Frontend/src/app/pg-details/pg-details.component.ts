import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { Slide } from '../slide';
import { MatDialog } from '@angular/material/dialog';
import { ContactDetailsDialogComponent } from '../contact-details-dialog/contact-details-dialog.component';
import { ToastrService } from 'ngx-toastr';

export interface Manager {
  managerId: any;
  firstName: string;
  lastName: string;
  emailId: string;
  mobileNumber: number;
  address: string;
}

export interface PG {
  pgId: number;
  imageName: string;
  subImage1: string;
  subImage2: string;
  licenseNumber: string;
  amenities: string;
  name: string;
  gender: string;
  location: string;
  sharesAvailable: string;
  priceForFour?: string;
  priceForTwo?: string;
  priceForThree?: string;
  vacancies2Share?: number;
  vacancies3Share?: number;
  vacancies4Share?: number;
  totalVacancies?: number;

  manager?: Manager; 
}

@Component({
  selector: 'app-pg-details',
  templateUrl: './pg-details.component.html',
  styleUrls: ['./pg-details.component.css']
})
export class PgDetailsComponent implements OnInit {
  pgImages: Slide[] = [];
  errorMessage: string | undefined;
  pg: PG | undefined;
length:any;
  constructor(private route: ActivatedRoute, private service: UserService, public dialog: MatDialog,private toastr:ToastrService) { }

  ngOnInit(): void {
    const pgId = this.route.snapshot.params['pgId'];

    if (pgId !== undefined) {
      this.service.getPgById(pgId).subscribe(
        (pg: PG) => {
          this.pg = pg;
          if (pg !== null) {
            if (pg.imageName) {
              this.pgImages.push({ image: './assets/Images/' + pg.imageName, title: pg.name, description: pg.location });
            }
            if (pg.subImage1) {
              this.pgImages.push({ image: './assets/Images/' + pg.subImage1, title: pg.name, description: pg.location });
            }
            if (pg.subImage2) {
              this.pgImages.push({ image: './assets/Images/' + pg.subImage2, title: pg.name, description: pg.location });
            }
          } else {
            console.error('PG not found');
            this.errorMessage = 'PG not found';
          }
        },
        (error) => {
          console.error('Error fetching PG details:', error);
          this.errorMessage = 'Error fetching PG details. Please try again later.';
        }
      );
    } else {
      console.error('pgId is undefined');
      this.errorMessage = 'PG ID is undefined';
    }
  }

  openContactDetailsDialog(): void {
    const dialogRef = this.dialog.open(ContactDetailsDialogComponent, {
      width: '400px',
      data: { manager: this.pg?.manager } 
    });
  }
  addToWishList() {
    if (this.pg) {
      const cartItems={
        
        name:this.pg.name,
        imageName:this.pg.imageName,
        location:this.pg.location,
        users:{
          userId:this.service.getUserId()
        }
      }
   
      this.service.toCart(cartItems).subscribe(
        response => {
          this.length=this.service.getCartLength();
          this.length=this.length++;
          this.service.setCartLength(this.length);
          console.log('PG added successfully:', response);
          this.toastr.success('Pg added to cart');
        },
        error => {
          console.error('Error adding PG:', error);
          this.toastr.error('Error Adding pg Details');
        }
      );
      // this.toastr.success('Pg added to your WishList')

    }
  }
  
}
