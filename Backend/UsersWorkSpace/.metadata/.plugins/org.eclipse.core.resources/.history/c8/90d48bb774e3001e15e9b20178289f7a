package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Users;
import com.mysql.cj.protocol.Message;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;

@Service
public class UsersDao {

	@Autowired
	UsersRepository usersRepository;
	@Autowired
	private JavaMailSender mailSender;
	private static final int OTP_LENGTH = 6;

	private static final String ACCOUNT_SID = "AC2f4bcb960b99e9126a5337740afc3525";
	private static final String AUTH_TOKEN = "a41f7e4d07d6bc4a5ee9c7c2329e46b4";
	private static final String TWILIO_PHONE_NUMBER = "+16592177740";

	static {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	public List<Users> getUsers() {
		return usersRepository.findAll();
	}

	public Users getUserById(int userId) {
		return usersRepository.findById(userId).orElse(null);
	}

	public Users getUserByName(String userName) {
		return usersRepository.findByName(userName);
	}

	private String generateOTP() {
		Random random = new Random();
		StringBuilder otp = new StringBuilder();

		for (int i = 0; i < OTP_LENGTH; i++) {
			otp.append(random.nextInt(10));
		}

		return otp.toString();
	}

	/*
	 * private void sendOtpViaSms(Users user) { try { Message message =
	 * Message.creator(new com.twilio.type.PhoneNumber(user.getMobileNumber()),
	 * Message message = Message.creator(new
	 * com.twilio.type.MobileNumber(user.getMobileNumber()), new
	 * com.twilio.type.MobileNumber(TWILIO_PHONE_NUMBER),
	 * "Your OTP for registration is: " + user.getOtp()).create();
	 * 
	 * System.out.println("OTP sent successfully via SMS."); } catch
	 * (ApiException e) { if (e.getCode() == 21614) { System.err.
	 * println("OTP not sent: Twilio trial accounts cannot send messages to unverified numbers."
	 * ); } else { System.err.println("Error sending OTP via SMS: " +
	 * e.getMessage()); } } }
	 */

	private void sendWelcomeEmail(Users user) {

		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(user.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + user.getFirstName() + ",\n\n"
				+ "Thank you for registering Welcome to Urban Nest, your gateway to comfortable and hassle-free living in urban areas!"
				+ "\n" +

				"We are thrilled to have you join our community. As a member of Urban Nest, you'll enjoy exclusive access to a wide range of PG accommodations, tailored to suit your needs. ");

		mailSender.send(message);
	}

	// adding user By encrypting password
	public Users addUsers(Users users) {
		String otp = generateOTP();
		users.setOtp(otp);
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPwd = bcrypt.encode(users.getPassword());
		users.setPassword(encryptedPwd);
		Users savedUser = usersRepository.save(users);
		// Send a welcome email
		sendWelcomeEmail(savedUser);
		// sendOtpViaSms(savedUser);

		return savedUser;
	}

	public Users fetchUserOtp(String otp) {
		Users user = usersRepository.findByOtp(otp);
		return user;
	}

	// UserLogin
	public Users userLogin(String emailId, String password) {
		// Find the employee by emailId
		Users user = usersRepository.findByEmailId(emailId);

		// Check if employee exists and the provided password matches the stored
		// password
		if (user != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, user.getPassword())) {
				// Passwords match, return the employee

				// Set user object as an attribute in the session
				// session.setAttribute("loggedInUser", new Boolean(true));
				// Object obj=(session.getAttribute("loggedInUser"));
				// System.out.println("Sesion Object value "+obj);

			}
			return user;
		}

		// Return null if login fails
		return null;
	}

	public Users updateUser(Users users) {
		return usersRepository.save(users);
	}

	public Users updateUserPassword(String emailId, String password) {
		Users users = usersRepository.findByEmailId(emailId);
		Users savedUser = null;
		if (users != null) {
			BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
			String encryptedPwd = bcrypt.encode(users.getPassword());
			users.setPassword(encryptedPwd);
			savedUser = usersRepository.save(users);
		}
		return savedUser;
	}

	public Users getUserEmailId(String emailId) {
		Users users = usersRepository.findByEmailId(emailId);
		if (users != null) {
			String otp = generateOTP();
			users.setOtp(otp);
			sendOtpEmail(users);
		}
		return users;
	}

	private void sendOtpEmail(Users users) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(users.getEmailId());
		message.setSubject("Password Reset OTP");
		message.setText("Dear " + users.getFirstName() + ",\n\n" + "Your OTP for password reset is: " + users.getOtp());

		mailSender.send(message);
	}

	public void deleteUserById(int userId) {
		usersRepository.deleteById(userId);
	}

}
