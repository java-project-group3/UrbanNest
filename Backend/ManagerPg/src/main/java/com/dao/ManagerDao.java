package com.dao;

/*import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Manager;*/






/*@Service*/
public class 	ManagerDao {

	/*@Autowired
	ManagerRepository managerRepository;
	@Autowired
	private JavaMailSender mailSender;

	public List<Manager> getManagers() {
		return managerRepository.findAll();
	}

	public Manager getManagerById(int managerId) {
		return managerRepository.findById(managerId).orElse(null);
	}

	public Manager getManagerByName(String managerName) {
		return managerRepository.findByName(managerName);
	}
      private void sendWelcomeEmail(Manager manager) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(manager.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + manager.getFirstName() + ",\n\n"
				+ "Thank you for registering Welcome to Urban Nest, your gateway to comfortable and hassle-free living in urban areas!"+"\n"+

                  "We are thrilled to have you join our community. As a member of Urban Nest, you'll enjoy exclusive access to a wide range of PG accommodations, tailored to suit your needs. ");

		mailSender.send(message);
	}
    //adding user By encrypting password
	public Manager addManager(Manager manager) {
		  BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
	        String encryptedPwd = bcrypt.encode(manager.getPassword());
	        manager.setPassword(encryptedPwd);
	        Manager savedManager =managerRepository.save(manager);
	        //Send a welcome email
			sendWelcomeEmail(savedManager);
		return savedManager;
	}
   //UserLogin
	public Manager managerLogin(String emailId, String password) {
	    // Find the employee by emailId
	    Manager manager = managerRepository.findByEmailId(emailId);

	    // Check if employee exists and the provided password matches the stored password
	    if (manager != null) {
	        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	        if (passwordEncoder.matches(password, manager.getPassword())) {
	            // Passwords match, return the employee
	            return manager;
	        }
	    }

	    // Return null if login fails
	    return null;
	}
	
	public Manager updateUser(Manager manager) {
		return managerRepository.save(manager);
	}

	public void deleteUserById(int managerId) {
		managerRepository.deleteById(managerId);
	}
	*/
	
}
