package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer>{
	/* @Query("from Manager where concat(firstName, ' ', lastName) = :userName")
	    Manager findByName(@Param("userName") String userName);
	    
	    @Query("from Users where emailId=:emailId")
	    Manager findByEmailId(@Param("emailId") String emailId);
	    
		@Query("from Users where emailId = :emailId and password = :password")
		Manager managerLogin(@Param("emailId") String emailId, @Param("password") String password);*/

}
