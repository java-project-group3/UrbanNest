package com.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.model.Pg;
@Repository

public class PgDao {
	
	@Autowired
    private PgRepository pgRepository;
	 public List<Pg> getPgs() {
	        return pgRepository.findAll();
	    }
	 public Pg getPgById(Long pgId) {
		 return pgRepository.findById(pgId).orElse(null);
	        
	    }
	 public Pg addPg(Pg pg) {
	        return pgRepository.save(pg);
	    }
	 public Pg updatePg(Pg pg) {
	        return pgRepository.save(pg);
	    }
	 
	 public void deletePgById(Long pgId) {
	        pgRepository.deleteById(pgId);
	    }

	 
	
}
