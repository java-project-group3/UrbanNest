package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Pg;

public interface PgRepository extends JpaRepository<Pg, Long> {
    
}
