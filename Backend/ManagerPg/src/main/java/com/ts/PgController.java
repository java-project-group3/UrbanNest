package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.PgDao;
import com.model.Pg;

@RestController

public class PgController {
	@Autowired
	PgDao pgDao;


	@GetMapping("getPgs")
	public List<Pg> getPgs() {
		return pgDao.getPgs();
	}
	
	@GetMapping("getPgById/{pgId}")
	public Pg getUserById(@PathVariable Long pgId) {
		return pgDao.getPgById(pgId);
	}
	@PostMapping("addPg")
	public Pg addPg(@RequestBody Pg pg) {
		return pgDao.addPg(pg);
	}
	@PutMapping("updatePg")
	public Pg updatePg(@RequestBody Pg pg) {
		return pgDao.updatePg(pg);
	}
	
	@DeleteMapping("deletePgById/{pgId}")
	public String deleteUserById(@PathVariable Long pgId) {
		pgDao.deletePgById(pgId);
		return "Pg With pgId:" + pgId + " Deleted Successfully!!!" ;
	}
	

}
