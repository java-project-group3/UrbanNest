package com.model;

import java.util.concurrent.atomic.AtomicLong;

public class PgIdGenerator {
	 private static final AtomicLong sequence = new AtomicLong(1000); // Initial value set to 1000

	    public static long getNextId() {
	        return sequence.incrementAndGet();
	    }

}
