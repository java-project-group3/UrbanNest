package com.model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Manager {
	@Id

    @GeneratedValue(strategy = GenerationType.IDENTITY) // Auto-increment
	private int managerId;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    
    private String emailId;
    private String  password;
    @Column(unique = true)

    private long mobileNumber;
    private String address;
    private String gender;
	//Implementing Mapping Between Manager and pg
	@JsonIgnore	//UnComment this later on
    @OneToMany(mappedBy = "manager", cascade = CascadeType.ALL)
    
    List<Pg> pgList = new ArrayList<Pg>();
	
   public  Manager(){
    	
    }
	public Manager(String firstName, String lastName, String emailId, String password, long mobileNumber,
			String address, String gender) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.password = password;
		this.mobileNumber = mobileNumber;
		this.address = address;
		this.gender = gender;
	}
	
	
	public List<Pg> getPgList() {
		return pgList;
	}
	public void setPgList(List<Pg> pgList) {
		this.pgList = pgList;
	}
	public int getManagerId() {
		return managerId;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Manager [managerId=" + managerId + ", firstName=" + firstName + ", lastName=" + lastName + ", emailId="
				+ emailId + ", password=" + password + ", mobileNumber=" + mobileNumber + ", address=" + address
				+ ", gender=" + gender + "]";
	}
	
    
}
