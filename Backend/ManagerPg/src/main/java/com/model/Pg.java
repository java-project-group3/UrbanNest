package com.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.Min;

import ENUM.Gender;

@Entity
public class Pg {
	@Id
	@GeneratedValue
	private Long pgId;
	

	   

	private String licenseNumber; // Starting from 1001

	private String name;

	private String location;

	private String sharesAvailable; // 2/3/4

	private double priceForTwo;

	private double priceForThree;

	private double priceForFour;

	@Enumerated(EnumType.STRING)
	private Gender gender; // Male, Female, Co-living
	@Min(value = 0, message = "Total vacancies cannot be negative")
	private int totalVacancies;

	@Min(value = 0, message = "Vacancies for 2 share cannot be negative")
	private int vacancies2Share;

	@Min(value = 0, message = "Vacancies for 3 share cannot be negative")
	private int vacancies3Share;

	@Min(value = 0, message = "Vacancies for 4 share cannot be negative")
	private int vacancies4Share;

	@JoinColumn(name = "managerId")
	@ManyToOne

	private Manager manager;
public Pg(){
	
}
	public Pg( String licenseNumber, String name, String location, String sharesAvailable, double priceForTwo,
			double priceForThree, double priceForFour, Gender gender,
			@Min(value = 0, message = "Total vacancies cannot be negative") int totalVacancies,
			@Min(value = 0, message = "Vacancies for 2 share cannot be negative") int vacancies2Share,
			@Min(value = 0, message = "Vacancies for 3 share cannot be negative") int vacancies3Share,
			@Min(value = 0, message = "Vacancies for 4 share cannot be negative") int vacancies4Share,
			Manager manager) {
		super();
	
		this.licenseNumber = licenseNumber;
		this.name = name;
		this.location = location;
		this.sharesAvailable = sharesAvailable;
		this.priceForTwo = priceForTwo;
		this.priceForThree = priceForThree;
		this.priceForFour = priceForFour;
		this.gender = gender;
		this.totalVacancies = totalVacancies;
		this.vacancies2Share = vacancies2Share;
		this.vacancies3Share = vacancies3Share;
		this.vacancies4Share = vacancies4Share;
		this.manager = manager;
	}

	public Long getPgId() {
		return pgId;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSharesAvailable() {
		return sharesAvailable;
	}

	public void setSharesAvailable(String sharesAvailable) {
		this.sharesAvailable = sharesAvailable;
	}

	public double getPriceForTwo() {
		return priceForTwo;
	}

	public void setPriceForTwo(double priceForTwo) {
		this.priceForTwo = priceForTwo;
	}

	public double getPriceForThree() {
		return priceForThree;
	}

	public void setPriceForThree(double priceForThree) {
		this.priceForThree = priceForThree;
	}

	public double getPriceForFour() {
		return priceForFour;
	}

	public void setPriceForFour(double priceForFour) {
		this.priceForFour = priceForFour;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getTotalVacancies() {
		return totalVacancies;
	}

	public void setTotalVacancies(int totalVacancies) {
		this.totalVacancies = totalVacancies;
	}

	public int getVacancies2Share() {
		return vacancies2Share;
	}

	public void setVacancies2Share(int vacancies2Share) {
		this.vacancies2Share = vacancies2Share;
	}

	public int getVacancies3Share() {
		return vacancies3Share;
	}

	public void setVacancies3Share(int vacancies3Share) {
		this.vacancies3Share = vacancies3Share;
	}

	public int getVacancies4Share() {
		return vacancies4Share;
	}

	public void setVacancies4Share(int vacancies4Share) {
		this.vacancies4Share = vacancies4Share;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	private void updateTotalVacancies() {
        this.totalVacancies = this.vacancies2Share + this.vacancies3Share + this.vacancies4Share;
    }

	@Override
	public String toString() {
		return "Pg [id=" + pgId + ", licenseNumber=" + licenseNumber + ", name=" + name + ", location=" + location
				+ ", sharesAvailable=" + sharesAvailable + ", priceForTwo=" + priceForTwo + ", priceForThree="
				+ priceForThree + ", priceForFour=" + priceForFour + ", gender=" + gender + ", totalVacancies="
				+ totalVacancies + ", vacancies2Share=" + vacancies2Share + ", vacancies3Share=" + vacancies3Share
				+ ", vacancies4Share=" + vacancies4Share + ", manager=" + manager + "]";
	}

}
