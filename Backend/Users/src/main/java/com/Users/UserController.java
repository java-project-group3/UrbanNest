package com.Users;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UsersDao;
import com.model.Users;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	
	@Autowired
	UsersDao usersDao;

	@GetMapping("getUsers")
	public List<Users> getUsers() {
		return usersDao.getUsers();
	}
	
	@GetMapping("getUserById/{userId}")
	public Users getUserById(@PathVariable int userId) {
		return usersDao.getUserById(userId);
	}
	
	@GetMapping("getUserByName/{userName}")
	public Users getUserByName(@PathVariable String userName) {
		return usersDao.getUserByName(userName);
	}
	
	@GetMapping("userLogin/{emailId}/{password}")
	public Users userLogin(@PathVariable String emailId, @PathVariable String password) {
		return usersDao.userLogin(emailId, password);
	}
	
	@PostMapping("addUser")
	public Users addUsers(@RequestBody Users users) {
		return usersDao.addUsers(users);
	}
	
	@PutMapping("updateUser")
	public Users updateUser(@RequestBody Users users) {
		return usersDao.updateUser(users);
	}
	
	@DeleteMapping("deleteUserById/{userId}")
	public String deleteUserById(@PathVariable int userId) {
		usersDao.deleteUserById(userId);
		return "User With userId:" + userId + " Deleted Successfully!!!" ;
	}
}


